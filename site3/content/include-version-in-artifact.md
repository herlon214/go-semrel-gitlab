---
title: Include version in jar or exe
description: You can query the commit log and embed the version number in jar or exe, even before the release has been made
weight: 34
---

# Include version in artifact

1. [determine the next version number](https://gitlab.com/juhani/go-semrel-gitlab/blob/4b1ea99782d6abb99b56c666ed4cc5eb5c22748f/.gitlab-ci.yml#L33)
   in the first job of the pipeline.
1. store the number in a file and 
   [declare it as an artifact](https://gitlab.com/juhani/go-semrel-gitlab/blob/4b1ea99782d6abb99b56c666ed4cc5eb5c22748f/.gitlab-ci.yml#L34-36)
1. [retrieve](https://gitlab.com/juhani/go-semrel-gitlab/blob/4b1ea99782d6abb99b56c666ed4cc5eb5c22748f/.gitlab-ci.yml#L44)
   the version number in subsequent phases of the pipeline 

### Note

- The artifact is available only in subsequent phases of the pipeline, not in the one it's defined in, or previous
- If you embed version number of the *potential* release to every build, pay attention to what happens to the artifacts of the
  pipelines that are not released. If the unreleased artifacts stay local to the pipeline, it should be safe. But if there
  is the possiblity that they are copied around, there is the possibility of confusion because there will be many builds with the
  same version number.
- Consider specifying [expiry](https://docs.gitlab.com/ee/ci/yaml/#artifactsexpire_in) for the artifacts
